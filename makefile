# You can modify the install path with
# `make install INSTALL_PATH=some-custom-path`
INSTALL_PATH=$(HOME)/.local/bin

# for install path of autocomplete, see:
# https://unix.stackexchange.com/questions/416185/where-to-install-bash-completion-scripts-for-out-of-tree-packages
COMPLETION_PATH=$(HOME)/.local/share/bash-completion/completions

install:
	install -D manta-build $(INSTALL_PATH)/manta-build
	install -D bash-completion/manta-build $(COMPLETION_PATH)/manta-build

uninstall:
	rm $(INSTALL_PATH)/manta-build
	rm $(COMPLETION_PATH)/manta-build
