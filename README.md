# Manta Build Tools

## Description

This repository gather some scripts (either python or bash) that may be useful
to automate the configuration and build process of the [Manta
library](https://gitlab.com/manta-project/manta). 

## Installation

To install `manta-build` command, change directory to the root of this repository and run

```
make install 
```

This does not require root permision. Scripts are installed in `$HOME/.local/`.

Note that this installation also provide autocompletion for `manta-build` command, for `bash` shell.

## Uninstall

To uninstall `manta-build` completely, change directory to the root of this repository and run

```
make uninstall
```

This does not require root permision. It will delete the `manta-build` scipt at `$HOME/.local` and the associated autocompletion file.

## Getting started

To get instruction about how to use `manta-build`, use the `--help` option:

```
manta-build --help
```

You can also use autocompletion with TAB.

## Support

Those tools are exclusively targetting GNU/Linux systems. For Windows users,
consider using tools such as WSL2. 
There is currently no support for macOS, but the portability should be
reasonable, as shell scripts are as POSIX compliant as possible.    

Any bug may can reported by opening an issue [on GitLab](https://gitlab.com/Gbkng/manta-build-tools/-/issues) 
To request help, please first search among issues first, and open an issue if no satisfactory answer
could be found.

## Roadmap

- [x] create an install procedure
- [x] provide autocompletion
- [x] improve this README with 'Installation' section
- [x] improve this README with 'Getting Started' section
- [ ] provide a man page and reduce `--help` output
- [ ] port existing bash script into python exclusively
- [ ] package the application as a pypackage

## Contributing

Feel free to suggest any improvment by opening an issue [on GitLab](https://gitlab.com/Gbkng/manta-build-tools/-/issues)

## License

This project has currently no associated license.


