#!/bin/bash
print-manta-env() {
    echo "\
CXX ${CXX}
CC ${CC}
FC ${FC}

OMPI_CXX ${OMPI_CXX}
OMPI_CC ${OMPI_CC}
OMPI_FC ${OMPI_FC}"
    return 0
}

_manta_utils_error_msg() {
  echo -e "ERROR:" "$@" >&2
}
_manta_utils_print_usage() {
  echo -e "\
USAGE
  manta-env --compiler {gcc,clang} --compiler-version INTEGER 
    [--help]
  "
}
# check if a given list of binaries exists
# $@ : list of binaries to check the existence for
# examples:
#       check_bin clang
#       check_bin clang gcc castem
_manta_utils_check_bin() {
  local no_bin_list=""

  for bin in "$@"; do
    # get full path to binary. 
    # command status != 0 if fails,
    # and returns empty string if fails
    
    local full_path
    full_path=$(command -v "${bin}")

    local commandStatus
    commandStatus=$?

    # if last command failed has failed, 
    # then binary unavailable in path 
    if [ ${commandStatus} -ne 0 ]; then
      no_bin_list="${no_bin_list}'${bin}' "
      # if available in path, 
      # also check if path has exe rights
    elif [ ! -x "${full_path}" ]; then
      no_bin_list="${no_bin_list} '${bin}'"
      fi
  done

  if [ "${no_bin_list}" ]; then
    _manta_utils_error_msg "${no_bin_list} not available in path."
    return 1
  else
    return 0
  fi
}
manta-env() {
    # set manta environment: set compilers and load spack


    # --------------------------------------
    # default values
    #
    gcc_c_compiler='gcc'
    gcc_cxx_compiler='g++'
    clang_c_compiler='clang'
    clang_cxx_compiler='clang++'


    # Default to gcc
    compiler='gcc'

    # Default to no compiler suffix
    compiler_version=''

    # --------------------------------------
    # argument parsing
    #
    temp=$(getopt \
      -o 'c:' \
      --long 'compiler:,compiler-version:' \
      -- "$@")

    if [ "$?" -ne 0 ]; then
      _manta_utils_error_msg 'Argument parsing failed.' >&2
      _manta_utils_print_usage
      return 2
    fi

    # Note the quotes around "$TEMP": they are essential!
    eval set -- "${temp}"
    unset TEMP

    # output with help if no argument given
    if [ "$1" = '--' ]; then
      _manta_utils_error_msg 'No argument given.' >&2
      _manta_utils_print_usage
      return 2
    fi

    # --------------------------------------
    # argument handling
    while true; do
      case "$1" in
        '--help'|'-h')
          print_help
          return 0
          ;;
        '-c'|'--compiler')
          compiler=$2
          shift 2; continue
          ;;
        '--compiler-version')
          compiler_version=$2
          shift 2; continue
          ;;
        '--')
          # echo 'end of argument handling'
          shift; break
          ;;
        *)
          _manta_utils_error_msg 'Arguments handling failed.'
          echo 'current arg:' "$1"
          echo 'remaining args (including current):' "$@" ''
          _manta_utils_print_usage
          return 2
          ;;
      esac
    done

    # check --compiler_version is an integer
    if [ -n "${compiler_version}" ]; then
      if [ "${compiler_version}" -ne "${compiler_version}" ] || [ "${compiler_version}" -le "0" ]; then 
        _manta_utils_error_msg "The compiler version must be an integer. You have set the option to '${compiler_version}'."
        return 2
      fi
      # if compiler_version specified, add the '-' char for 
      # prefixing compiler name correctly (gcc and clang 
      # version convention)
      compiler_version="-${compiler_version}"
    fi

    # check --compiler option has an expected value
    if [ "${compiler}" = 'gcc' ]; then 
      OMPI_CC="${gcc_c_compiler}${compiler_version}"
      OMPI_CXX="${gcc_cxx_compiler}${compiler_version}"
    elif [ "${compiler}" = 'clang' ] ; then
      OMPI_CC="${clang_c_compiler}${compiler_version}"
      OMPI_CXX="${clang_cxx_compiler}${compiler_version}"
    else
      _manta_utils_error_msg "The compiler option '-c' must be either 'gcc' or 'clang'. You have set the option to '${compiler}'."
      return 2
    fi

    OMPI_FC='gfortran'

    CXX='mpicxx'
    CC='mpicc'
    FC='mpifort'

    # check if the various compilers and wrappers are available in path
    if ! _manta_utils_check_bin "${OMPI_CC}" "${OMPI_CXX}" "${OMPI_FC}" "${CXX}" "${CC}" "${FC}"; then
      _manta_utils_error_msg "Not all required binaries are available in path."
      return 2
    fi

    # if all binaries are available, set environment variables
    export OMPI_CXX="${OMPI_CXX}"
    export OMPI_CC="${OMPI_CC}"
    export OMPI_FC="${OMPI_FC}"

    export CXX="${CXX}"
    export CC="${CC}"
    export FC="${FC}"

    print-manta-env

    echo "loading spack environment"
    source "${HOME}/spack/share/spack/setup-env.sh"

    return 0
}
